import os
import json
import time
import shutil

import boto3
import requests
import pytest

from bitbucket_pipes_toolkit.test import PipeTestCase
from bitbucket_pipes_toolkit.helpers import get_variable


STACK_NAME = f"bbci-pipes-test-infrastructure-lambda-{get_variable('BITBUCKET_BUILD_NUMBER')}"
AWS_DEFAULT_REGION = 'us-east-1'
ZIP_FILE = './test/lambda.zip'
APP_CONFIG_FILE = './test/sam-app/config.json'
LAMBDA_CONFIG_FILE = 'test/function-configuration.json'


class AwsLambdaManager:
    def __init__(self):
        self.stack_name = STACK_NAME
        self.client_cloudformation = boto3.client('cloudformation',
                                   region_name=AWS_DEFAULT_REGION)
        self.client_lambda = boto3.client('lambda',
                                           region_name=AWS_DEFAULT_REGION)

    def get_lambda_function_name(self):
        return self.get_stack_output('Function')['OutputValue']

    def get_lambda_function_configuration(self):
        function_name = self.get_lambda_function_name()
        return self.client_lambda.get_function_configuration(
            FunctionName=function_name)

    def get_lambda_function_alias(self, alias):
        function_name = self.get_lambda_function_name()
        return self.client_lambda.get_alias(
            FunctionName=function_name, Name=alias)

    def get_lambda_function_role_arn(self):
        return self.get_stack_output('FunctionIamRole')['OutputValue']

    def get_lambda_api(self):
        return self.get_stack_output('Api')['OutputValue']

    def get_stack_output(self, output_type, output_name='HelloWorld'):
        stack = self.get_stack(self.stack_name)
        return [item for item in stack['Outputs'] if item[
            'OutputKey'] == f'{output_name}{output_type}'][0]

    def get_stack(self, stack_name):
        response = self.client_cloudformation.describe_stacks(StackName=stack_name)
        return response['Stacks'][0]


class LambdaDeployFunctionTestCase(PipeTestCase):
    ALIASES = []

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()
        ALIASES = []

    def setUp(self):
        self.random = str(time.time_ns())
        self.aws_lambda_manager = AwsLambdaManager()
        self.update_app_configuration_file(self.random)
        self.create_zip_lambda_app()

    def tearDown(self):
        pass

    @staticmethod
    def create_zip_lambda_app():
        cwd = os.getcwd()
        os.chdir('test')
        shutil.make_archive('lambda', 'zip', './sam-app/')
        os.chdir(cwd)

    @staticmethod
    def update_app_configuration_file(message, config_file=APP_CONFIG_FILE):
        with open(config_file, 'w') as f:
            f.write(json.dumps({"message": str(message)}))

    @staticmethod
    def update_lambda_configuration_file(text, config_file=LAMBDA_CONFIG_FILE):
        with open(config_file, 'w') as f:
            f.write(json.dumps({"Description": str(text)}))

    def test_fail_if_no_params(self):
        result = self.run_container()
        self.assertRegex(result, rf'AWS_ACCESS_KEY_ID variable missing')

    def test_success_update(self):
        result = self.run_container(
            environment={
                'AWS_ACCESS_KEY_ID': get_variable('AWS_ACCESS_KEY_ID'),
                'AWS_SECRET_ACCESS_KEY': get_variable('AWS_SECRET_ACCESS_KEY'),
                'AWS_DEFAULT_REGION': AWS_DEFAULT_REGION,
                'FUNCTION_NAME':
                    self.aws_lambda_manager.get_lambda_function_name(),
                'ZIP_FILE': ZIP_FILE,
                'COMMAND': "update",
            }
        )
        self.assertIn('Update command succeeded', result)
        response = requests.get(self.aws_lambda_manager.get_lambda_api())
        self.assertIn(self.random, response.json().get('message'))

    def test_success_update_configuration(self):
        self.update_lambda_configuration_file(self.random)
        result = self.run_container(
            environment={
                'AWS_ACCESS_KEY_ID': get_variable('AWS_ACCESS_KEY_ID'),
                'AWS_SECRET_ACCESS_KEY': get_variable('AWS_SECRET_ACCESS_KEY'),
                'AWS_DEFAULT_REGION': AWS_DEFAULT_REGION,
                'FUNCTION_NAME':
                    self.aws_lambda_manager.get_lambda_function_name(),
                'ZIP_FILE': ZIP_FILE,
                'COMMAND': "update",
                'FUNCTION_CONFIGURATION': LAMBDA_CONFIG_FILE,
            }
        )
        self.assertIn('Lambda configuration update succeeded', result)

        self.assertIn(
            self.random,
            self.aws_lambda_manager.get_lambda_function_configuration()[
                'Description']
        )

    @pytest.mark.second_to_last
    def test_success_new_alias(self):
        alias = (f"myalias-{get_variable('BITBUCKET_BUILD_NUMBER')}"
                 f"-{self.random}")
        version = self.aws_lambda_manager.get_lambda_function_configuration()[
                'Version']
        function_name = self.aws_lambda_manager.get_lambda_function_name()

        result = self.run_container(
            environment={
                'AWS_ACCESS_KEY_ID': get_variable('AWS_ACCESS_KEY_ID'),
                'AWS_SECRET_ACCESS_KEY': get_variable('AWS_SECRET_ACCESS_KEY'),
                'AWS_DEFAULT_REGION': AWS_DEFAULT_REGION,
                'FUNCTION_NAME': function_name,
                'COMMAND': "alias",
                'ALIAS': alias,
                'VERSION': version,
            }
        )
        self.ALIASES.append(alias)
        self.assertIn('Successfully created alias', result)
        response = self.aws_lambda_manager.get_lambda_function_alias(alias)
        self.assertIn(alias, response.get('Name'))

    @pytest.mark.last
    def test_success_update_exist_alias(self):
        client = self.aws_lambda_manager.client_lambda
        version = self.aws_lambda_manager.get_lambda_function_configuration()[
                'Version']
        function_name = self.aws_lambda_manager.get_lambda_function_name()
        aliases = client.list_aliases(FunctionName=function_name,
                                      FunctionVersion=version)['Aliases']
        # alias = aliases[0]['Name']
        alias = self.ALIASES[0]

        result = self.run_container(
            environment={
                'AWS_ACCESS_KEY_ID': get_variable('AWS_ACCESS_KEY_ID'),
                'AWS_SECRET_ACCESS_KEY': get_variable('AWS_SECRET_ACCESS_KEY'),
                'AWS_DEFAULT_REGION': AWS_DEFAULT_REGION,
                'FUNCTION_NAME': function_name,
                'COMMAND': "alias",
                'ALIAS': alias,
                'VERSION': version,
            }
        )
        self.assertIn('Successfully updated alias', result)

        response = self.aws_lambda_manager.get_lambda_function_alias(alias)
        self.assertIn(alias, response.get('Name'))
