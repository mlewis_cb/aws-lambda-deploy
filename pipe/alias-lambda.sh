#!/bin/bash

aws_extra_args=""
if [[ "${DEBUG}" == true ]]; then
    aws_extra_args="--debug"
fi

create_alias() {
      info "Existing alias not found - creating new alias."

      # Create alias if does not exist.
      run aws lambda create-alias \
          --function-name "${FUNCTION_NAME}" \
          --function-version "${VERSION}" \
          --name "${ALIAS}" \
          ${aws_extra_args}
      if [[ "${status}" == "0" ]]; then
          success "Successfully created alias."
      else
          fail "Failed to create alias"
      fi
}

update_alias() {
     info "Existing alias found - updating alias."
     run aws lambda update-alias \
         --function-name "${FUNCTION_NAME}" \
         --function-version "${VERSION}" \
         --name "${ALIAS}" \
         ${aws_extra_args}

     if [[ "${status}" == "0" ]]; then
        success "Successfully updated alias."
     else
         fail "Failed to update alias"
     fi
}

get_alias() {
  # Fetch existing alias
  info "Checking for existing lambda alias."
  run aws lambda get-alias --function-name "${FUNCTION_NAME}" --name "${ALIAS}" ${aws_extra_args}
}

alias() {
  get_alias
  if [[ "$status" -eq "0" ]]; then
    update_alias
  else
    create_alias
  fi
}
