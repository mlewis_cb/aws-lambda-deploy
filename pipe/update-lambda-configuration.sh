aws_extra_args=""
if [[ "${DEBUG}" == true ]]; then
    aws_extra_args="--debug"
fi

## Upload and publish the lambda fuction atomically.
update_lambda_configuration() {
  info "Updating Lambda function configuration."
  function_configuration=$(cat "${FUNCTION_CONFIGURATION}")
  run aws lambda update-function-configuration  --function-name "${FUNCTION_NAME}" --cli-input-json "${function_configuration}"
  if [[ "${status}" -ne "0" ]]; then
    fail "Failed to update Lambda function configuration."
  fi
  info "Lambda configuration update succeeded."
}